import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.*;
import java.util.Scanner;

public class CsvToXl {
    public static final char FILE_DELIMITER = ',';
    public static final String FILE_EXTN = ".xlsx";
    public static final String FILE_NAME = "EXCEL_DATA";

    public static void main(String[] args) throws IOException {
        cleanCsvFile();
        String generatedXlsFilePath = "";
        generatedXlsFilePath = FILE_NAME + FILE_EXTN;

        try (
                Scanner sc = new Scanner(new File("ndip_non_affecte.csv"));
                Workbook workBook = new SXSSFWorkbook();
                FileOutputStream fileOutputStream = new FileOutputStream(generatedXlsFilePath.trim());
        ) {
            CellStyle cellStyle = workBook.createCellStyle();
            cellStyle.setWrapText(true);
            /**** Get the CSVReader Instance & Specify The Delimiter To Be Used ****/
            String[] nextLine;
            Sheet sheet = workBook.createSheet("Sheet");
            int rowNum = 0;
            while (sc.hasNext()) {
                Row currentRow = sheet.createRow(rowNum++);
                String[] tab = sc.next().split(",");
                for (int i = 0; i < tab.length; i++) {
                    var cell = currentRow.createCell(i);
                    cell.setCellValue((tab[i]));
                    cell.setCellStyle(cellStyle);
                }
            }
            workBook.write(fileOutputStream);
        }
    }

    private static void cleanCsvFile() throws IOException {
        StringBuilder contentBuilder = new StringBuilder();
        String fileName = "ndip_non_affecteModified.csv";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
             Scanner sc = new Scanner(new File("ndip_non_affecte.csv"));) {
            sc.useDelimiter("/r/n");   //sets the delimiter pattern
            while (sc.hasNext()) {
                String ligne = sc.next();
                ligne.replace("/r", "");
                contentBuilder.append(ligne);
            }
            writer.append(contentBuilder);
        }
    }
}




